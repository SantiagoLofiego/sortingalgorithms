package Examen;

import java.util.logging.Level;
import java.util.logging.Logger;

public class QuickSort {

    public static void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private static void sort(int[] array, int start, int end) {
        if (start >= end) return;
        //int pivotIndex=new Random().nextInt(end-start)+start;
        int pivot = array[end];
        //swap(array,pivotIndex,end);
        int leftPointer = start;
        int rightPointer = end;

        while (leftPointer < rightPointer) {
            while (array[leftPointer] <= pivot && leftPointer < rightPointer) {
                leftPointer++;
            }
            while (array[rightPointer] >= pivot && leftPointer < rightPointer) {
                rightPointer--;
            }
            if (leftPointer < rightPointer) {
                swap(array, leftPointer, rightPointer);
            }
        }
        swap(array, end, leftPointer);

        sort(array, start, leftPointer - 1);
        sort(array, leftPointer + 1, end);
    }

    private static void swap(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    public static void prueba() {
        /* FIXME */
        String hola="hola";
        hola.charAt(3);
        Logger log =Logger.getLogger("actividad.log");
        log.log(Level.FINER,"TROLO");
    }
}
