package Examen;

import java.util.Arrays;

public class MergeSort {

    public static void mergeSort(int [] array){
        int size = array.length;
        int mid= array.length /2;
        if(size<2){
            return;
        }

        int[]leftArray=new int[mid];
        int[]rightArray= new int[size-mid];

        for (int i = 0; i < leftArray.length; i++) {
            leftArray[i]= array[i];
        }

        for (int i = mid; i < array.length; i++) {
            rightArray[i-mid]= array[i];
        }
        /*print(leftArray);
        print(rightArray);*/

        mergeSort(leftArray);
        mergeSort(rightArray);

        merge(array,leftArray,rightArray);

    }

    private static void merge(int[] array, int[] leftArray, int[] rightArray) {
        int i = 0, j = 0, k=0;
        while (i<leftArray.length && j<rightArray.length){
            if(leftArray[i]<=rightArray[j]){
                array[k]=leftArray[i];
                i++;
            }else{
                array[k]=rightArray[j];
                j++;
            }
            k++;
        }
        int rest= i>j?i : j;
        while (i<leftArray.length){
            array[k]=leftArray[i];
            k++;
            i++;
        }
        while (j<rightArray.length){
            array[k]=rightArray[j];
            k++;
            j++;
        }
    }

    public static void print(int[]array){
        System.out.println(Arrays.toString(array));
    }
}
