package Examen;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class Test {
    public static void main(String[] args) {
        String input1="3 3 2 1 3";
        String input2="8";
        List<Integer>list1= Arrays.stream(input1.split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer>list2=Arrays.stream(input2.split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer>list3=Arrays.asList(5,2);
        List<Integer>list4=Arrays.asList(4,2);
        List<Integer>list5=Arrays.asList(2,3);
        List<List<Integer>>matrix= Arrays.asList(list3,list4, list5);

        /*long startTime = System.nanoTime();
        List<Long> fibonacci = (Stream.iterate(new long[]{0, 1}, t -> new long[]{t[1], t[0] + t[1]})).limit(60).map(t->t[0]).collect(Collectors.toList());
        for (int i = 0; i < 60; i++) {
            System.out.print(fib(i));
            System.out.print("   ||   ");
            System.out.print(fibWmemo(i,new HashMap<>()));
            System.out.print("   ||   ");
            System.out.print(fibIter(i));
            System.out.print(fibonacci.get(i));
            System.out.print("---->" +i +"\n");
        }
        long endTime = System.nanoTime();
        System.out.println((endTime-startTime)/1000000.0);*/
        Random random= new Random();
        System.out.println("CREATING ARRAY.....");
        int[] numbers = new int[100000000];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i]=random.nextInt(100000000);
        }
        int[]array={1,2,85,32,24,77,98,15,65,48,33,35,7,7,7,18,7};



        //MergeSort.print(numbers);
        long start= System.nanoTime();
        System.out.println("SORTING....");
        //MergeSort.mergeSort(numbers);
        //Arrays.sort(numbers);
        //BtreeSort.sort(numbers);
        //QuickSort.sort(numbers);
        QuickSort.prueba();

        System.out.println("FINISH");
        long end= System.nanoTime();

        //MergeSort.print(numbers);

        System.out.println("TIME TO SOLVE (ms) = " + (end - start)/1000000 );




    }
    public static String biggerIsGreater(String w) {
        String result="";
        String[] array=w.split("");
        int subfix = array.length-1;
        int pivot = 0;
        int swap=array.length-1;
        while(subfix>0 && array[subfix].compareTo(array[subfix-1])<=0){
            subfix--;
        }
        if(subfix==0){
            return "no answer";
        }else {
            pivot=subfix-1;
        }
        while(array[pivot].compareTo(array[swap])>=0){
            swap--;
        }
        System.out.println(array[pivot]);
        System.out.println(array[swap]);
        String temp= array[pivot];
        array[pivot]=array[swap];
        array[swap]=temp;
        for (int i = 0; i < subfix; i++) {
            result+=array[i];
        }
        for (int i = array.length-1; i > pivot ; i--) {
            result+= array[i];
        }
        return result;
    }

    public static String encryption(String s) {
        int sqrt= (int) Math.sqrt(s.length());
        int columns=sqrt*sqrt<s.length()?sqrt+1:sqrt;
        int rows= sqrt*columns<s.length()?sqrt+1:sqrt;
        String[]sList=s.split("");
        String result="";

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                int index= j*columns+i;
                if(index<sList.length){
                    result+=sList[index];
                }
            }
            if((i!=columns-1)){
                result+=" ";
            }
        }

        return result;
    }

    public static int queensAttack(int n, int k, int y_q, int x_q, List<List<Integer>> obstacles) {
        Map<String,Integer>distance=new HashMap<>();
        distance.put("UP", n-y_q);
        distance.put("DOWN",y_q-1);
        distance.put("LEFT",x_q-1);
        distance.put("RIGHT", n-x_q);
        distance.put("UR",Math.min(distance.get("UP"), distance.get("RIGHT")));
        distance.put("UL",Math.min(distance.get("UP"), distance.get("LEFT")));
        distance.put("DR",Math.min(distance.get("DOWN"), distance.get("RIGHT")));
        distance.put("DL",Math.min(distance.get("DOWN"), distance.get("LEFT")));

        for(List<Integer>pown: obstacles){
            int y=pown.get(0);
            int x=pown.get(1);
            int distPown=0;
            if(x==x_q){
                distPown=y-y_q;
                if(distPown>0 && distPown<distance.get("UP")){
                    distance.put("UP", distPown-1);
                }else if(Math.abs(distPown)<distance.get("DOWN")){
                    distance.put("DOWN",Math.abs(distPown)-1);
                }
            }
            if(y==y_q){
                distPown=x-x_q;
                if(distPown>0 && distPown<distance.get("RIGHT")){
                    distance.put("RIGHT", distPown-1);
                }else if(Math.abs(distPown)<distance.get("LEFT")){
                    distance.put("LEFT",Math.abs(distPown)-1);
                }
            }
            if(Math.abs(y-y_q)==Math.abs(x-x_q)){
                int distX=x-x_q;
                int distY=y-y_q;
                distPown=Math.min(Math.abs(distY), Math.abs(distX));
                if( distX> 0&& distY>0){
                    //UR
                    if(distPown<=distance.get("UR"))distance.put("UR", distPown-1);
                }else if(distX<0 && distY>0){
                    //UL
                    if(distPown<=distance.get("UL"))distance.put("UL", distPown-1);
                }else if(distX>0 && distY<0){
                    //DR
                    if(distPown<=distance.get("DR"))distance.put("DR", distPown-1);
                }else if(distX<0 && distY<0){
                    //DL
                    if(distPown<=distance.get("DL"))distance.put("DL", distPown-1);
                }
            }
        }


        return distance.values().stream().mapToInt(Integer::valueOf).sum();
    }
    public static int equalizeArray(List<Integer> arr) {
        Map<Integer,Integer>map=new HashMap<>();
        int result=0;
        for (int i = 0; i < arr.size(); i++) {
            int num=arr.get(i);
            if(map.containsKey(num)){
                result++;
            }else{
                map.put(num,1);
            }
        }
        return result;

    }

    public static int jumpingOnClouds(List<Integer> c) {
        int jumps = 0;
        for (int i = 0; i < c.size(); ) {
            boolean twoSp = i + 2 <= c.size()-1 && c.get(i + 2) == 0;
            boolean oneSp = i + 1 <= c.size()-1 && c.get(i + 1) == 0;
            if (twoSp) {
                i += 2;
                jumps++;
            } else if (oneSp) {
                i++;
                jumps++;
            } else {
                break;
            }
        }
        return jumps;
    }
    public static int nonDivisibleSubset(int k, List<Integer> s) {
        int[]remainders=new int[k];
        int counter=0;
        for(int n : s){
            remainders[n%k]++;
        }
        counter=remainders[0]>0?1:0;

        for (int i = 1; i <= k/2 ; i++) {
            if(i!=k-i){
                counter+=Math.max(remainders[i],remainders[k-i]);
            }else{
                counter+=remainders[i]>0?1:0;
            }
        }

        return counter;
    }

    public static List<Integer> cutTheSticks(List<Integer> arr) {
        List<Integer> result= new ArrayList<>();
        Collections.sort(arr);
        System.out.println(arr);
        result.add(arr.size());
        for (int i = 1; i < arr.size(); i++) {
            if(!Objects.equals(arr.get(i), arr.get(i - 1))){
                result.add(arr.size()-i);
            }
        }
        return result;
    }
    public static String appendAndDelete(String s, String t, int k) {
        if(k-s.length()-t.length()>0)return "Yes";
        int dif =Math.abs(t.length()-s.length());
        int min=Math.min(s.length(), t.length());
        int i = 0;
        for (; i < min; i++) {
            if(s.charAt(i)!=t.charAt(i)){
                break;
            }

        }
        System.out.println("I=" +i);
        k=k-dif-((min-i)*2);
        System.out.println(k);
        if(k==0||(k%2==0 && k>0)||k>i*2){
            return "Yes";
        }else{
            return "No";
        }
    }
    public static void extraLongFactorials(int n) {
        BigInteger num= BigInteger.valueOf(1);
        for (int i = 1; i <=n ; i++) {
            num=num.multiply(BigInteger.valueOf(i));
        }
        System.out.println(num);
    }

    public static BigInteger factorial(BigInteger n){
        if(n.equals(BigInteger.valueOf(1))){
            return BigInteger.valueOf(1);
        }else{
            return n.multiply(factorial(n.subtract(BigInteger.ONE)));
        }
    }

    static int jumpingOnClouds(int[] c, int k) {
        int e=100;
        int n=c.length;
        int i=k%n;
        e-=c[i]*2+1;
        System.out.println(("i="+i +"  c[i]= "+ c[i]));
        while(i!=0){
            i=(i+k)%n;
            e-=c[i]*2+1;
            System.out.println(("i="+i +"  c[i]= "+ c[i]));
        }
        return e;
    }
    public static List<Integer> circularArrayRotation1(List<Integer> a, int k, List<Integer> queries) {
        List<Integer>result=new ArrayList<>();

        for (int i = 0; i < queries.size() ; i++) {
            int index=queries.get(i);
            int index2= index-(k%a.size());
            if(index2<0)index2=a.size()+index2;
            result.add(a.get(index2));
        }
        return result;
    }
    public static List<Integer> circularArrayRotation(List<Integer> a, int k, List<Integer> queries) {
        Map<Integer,Integer>positions=new HashMap<>();
        List<Integer>result=new ArrayList<>();
        for (int i = 0; i < a.size() ; i++) {
            int index=i+k;
            if(index>a.size()-1)index=index-a.size();
            positions.put(index,a.get(i));
        }
        System.out.println(positions);
        for (int i = 0; i < queries.size() ; i++) {
            int index=queries.get(i);
            result.add(positions.get(index));
        }
        return result;
    }

    public static int saveThePrisoner(int n, int m, int s) {
        int rest= m%n;
        int position= s+rest-1;
        if(rest==0)position=s+(n)-1;
        if(position>n)position-=n;
        return position;
    }
    public static int beautifulDays(int i, int j, int k) {
        int counter=0;
        for (; i <= j ; i++) {
            int result = (i-reverse(i))%k;
            if(result==0)counter++;
        }
        return counter;
    }

    public static int reverse(int n){
        StringBuilder num2= new StringBuilder();
        num2.append(n);

        return Integer.parseInt(num2.reverse().toString());
    }

    public static int utopianTree(int n) {


        return (int)Math.pow(2,((n + (n%2))/2) + 1)-((n%2)+1);
    }

    public static int designerPdfViewer(List<Integer> h, String word) {
        Map<Character,Integer>map= new HashMap<>();
        int wordSize=word.length();
        int max=0;
        int i=0;
        for(char c='a';c<='z';c++){
            map.put(c,h.get(i));
            i++;
        }
        for (int j = 0; j < wordSize; j++) {
            char ch=word.charAt(j);
            if(map.get(ch)>max){
                max=map.get(ch);
            }
        }
        return wordSize*max;
    }

    public static List<Integer> climbingLeaderboard(List<Integer> ranked, List<Integer> player) {
        List<Integer>result=new ArrayList<>();
        List<Integer>ranking=new ArrayList<>();
        int position =1;
        for (int i = 0; i < ranked.size(); i++) {
            if(i!=0){
                if(ranked.get(i)<ranked.get(i-1)){
                    position++;
                }
                ranking.add(position);
            }else{
                ranking.add(position);
            }
        }
        System.out.println(ranking);
        for (int i = 0; i < player.size() ; i++) {
            if(player.get(i)>ranked.get(0)){
                result.add(1);
            }else if(player.get(i)<ranked.get(ranked.size()-1)){
                result.add((ranking.get(ranking.size()-1))+1);
            }else {
                result.add(ranking.get(bSearch(ranked,player.get(i),0,ranked.size()-1)));
            }
        }
        return result;
    }

    public static int bSearch(List<Integer> arr,int key,int start,int end){
        int mid=(end-start)/2+start;
        if(start<end) {
            if (key == arr.get(mid)){
                return mid;
            }
            else if (key > arr.get(mid)){
                return bSearch(arr, key, start, mid - 1);
            }
            else {
                return bSearch(arr, key, mid + 1, end);
            }
        }else {
            if(key< arr.get(start))return start+1;
            else return start;
        }
    }

    public static int pickingNumbers(List<Integer> a) {
        Map<Integer,Integer>map=new HashMap<>();
        final int[] result={0};
        for(int num : a){
            if(map.get(num)==null){
                map.put(num,1);
            }else{
                map.put(num,map.get(num)+1);
            }
        }
        map.forEach((k,v)->{
            int total=v;
            try{
                total+=map.get(k+1);
            }catch (Exception e){
            }
            if(total>result[0])result[0]=total;
        });
        return result[0];
    }

    public static int factorial(int n){
        if(n==0)return 0;
        return n*factorial(n-1);
    }

    public static int fib(int n){
        if(n==0){

            return 0;
        }
        if(n==1){

            return 1;
        }
        return fib(n-1)+fib(n-2);
    }
    public static long fibWmemo(long n,Map<Long,Long>memo){
        if(n==0){

            return 0;
        }
        if(n==1){

            return 1;
        }
        if(memo.get(n)!=null){
            return memo.get(n);
        }else{
            memo.put(n,fibWmemo(n-1,memo)+fibWmemo(n-2,memo));
            return memo.get(n);
        }
    }

    public static long fibIter(int n){
        if(n==0) {
            return 0;
        }else if(n==1){
            return 1;
        }
        long num1=0;
        long num2=1;
        long result=0;
        for (int i=2;i<=n;i++){
            result=num1+num2;
            num1 = num2;
            num2 = result;

        }
        return result;
    }

    public static void towerHanoi(int n, int from, int end){
        int aux=6-(from+end);
        if(n==1){
            System.out.println(from + " -> " +end);
            return;
        }else {
            towerHanoi(n-1,from,aux);
            System.out.println(from + " -> "+ end);
            towerHanoi(n-1,aux,end);
        }
    }

    public static int formingMagicSquare(List<List<Integer>> s) {
        int min=0;
        int[][] magicSquare =
                {
                        {4, 9, 2, 3, 5, 7, 8, 1, 6},
                        {4, 3, 8, 9, 5, 1, 2, 7, 6},
                        {2, 9, 4, 7, 5, 3, 6, 1, 8},
                        {2, 7, 6, 9, 5, 1, 4, 3, 8},
                        {8, 1, 6, 3, 5, 7, 4, 9, 2},
                        {8, 3, 4, 1, 5, 9, 6, 7, 2},
                        {6, 7, 2, 1, 5, 9, 8, 3, 4},
                        {6, 1, 8, 7, 5, 3, 2, 9, 4},
                };
        int[] mArray={
                s.get(0).get(0),s.get(0).get(1),s.get(0).get(2),
                s.get(1).get(0),s.get(1).get(1),s.get(1).get(2),
                s.get(2).get(0),s.get(2).get(1),s.get(2).get(2)
        };
        for (int i = 0; i < 8 ; i++) {
            int total=0;
            for (int j = 0; j < 9 ; j++) {
                total+=Math.abs(magicSquare[i][j]-mArray[j]);
            }
            if (i==0){
                min=total;
            }else if(total<min){
                min=total;
            }
        }
        return min;
    }

    public static int countingValleys(int steps, String path) {
        int valleys=0;
        int level=0;
        String[]pathlist=path.split("");
        for(String step : pathlist){
            int nextStep = step.equals("U")?1 : -1;
            if(level==0 && nextStep==-1)valleys++;
            level+=nextStep;
        }
        return valleys;
    }

    public static int pageCount(int n, int p) {
        int turnsTopage = (int)Math.floor(p/2.0);
        int totalTurns= (int)Math.floor(n/2.0);
        if(turnsTopage> totalTurns-turnsTopage){
            return totalTurns - turnsTopage;
        }else return turnsTopage;
    }

    public static int sockMerchant(int n, List<Integer> ar) {
        int pairs = 0;
        Set<Integer>colors=new HashSet<>();
        for(int sock : ar){
            if(!colors.contains(sock)){
                colors.add(sock);
            }else{
                pairs++;
                colors.remove(sock);
            }

        }

        return pairs;
    }


    public static String dayOfProgrammer(int year) {
        boolean leap=false;
        boolean transition=false;
        boolean gregorian=false;
        String day="13";
        String month="09";
        String date;
        if(year>1918)gregorian=true;
        if(gregorian && (year%400==0 || (year%4==0 && year%100!=0)))leap=true;
        if(!gregorian && year%4==0)leap=true;
        if(year==1918)transition=true;
        if(transition){
            day="26";
        }else if(leap){
            day="12";
        }
        date=day+"."+month+"."+year;
        return date;

    }

    public static int migratoryBirds(List<Integer> arr) {
        int [] counter = {0,0,0,0,0,0};
        int [] max={0,0};
        for (int id : arr) {
            counter[id]++;
            if(counter[id]> max[0]){
                max[0]=counter[id];
                max[1]=id;
            }else if(counter[id]==max[0]){
                if(id<max[1]){
                    max[0]=counter[id];
                    max[1]=id;
                }
            }
        }
        return max[1];
    }

    public static int divisibleSumPairs(int n, int k, List<Integer> ar) {
        int result=0;
        for(int i=0;i<n;i++){
            for(int j=i+1; j<n;j++){
                if((ar.get(i)+ar.get(j))%k==0){
                    result++;
                }
            }
        }
        return result;
    }

    public static int getTotalX(List<Integer> a, List<Integer> b) {
        int count=0;
        for (int i = 1; i <= 100; i++) {
            int finalI = i;
            if(a.stream().allMatch(n->finalI%n==0)){
                if(b.stream().allMatch(n->n%finalI==0)){
                    count++;
                }
            }

        }
        return count;

    }

    public static String kangaroo(int x1, int v1, int x2, int v2) {
        if(v1>v2){
            if((x1-x2)%(v2-v1)==0){
                return "YES";
            }
        }
        return "NO";
    }

    public static void countApplesAndOranges(int s, int t, int a, int b, List<Integer> apples, List<Integer> oranges) {
        int applesOnHouse=0;
        int orangesOnHouse=0;
        for (int i = 0; i < apples.size() ; i++) {
            int applePoint= a + apples.get(i);
            if (applePoint>=s && applePoint<=t){
                applesOnHouse++;
            }
        }
        for (int i = 0; i < oranges.size() ; i++) {
            int orangePoint= b + oranges.get(i);
            if (orangePoint>=s && orangePoint<=t){
                orangesOnHouse++;
            }
        }
        System.out.println(applesOnHouse);
        System.out.println(orangesOnHouse);
    }

    public static List<Integer> gradingStudents(List<Integer> grades) {
        List<Integer> result = grades.stream().map(g -> {
            int next5= ((g/5)+1)*5;
            if (g < 38) {
                return g;
            } else if(next5 - g>3){
                return g;
            }else  return next5;
        }).collect(Collectors.toList());
        return result;
    }

    public static String timeConversion(String s) {
        String[] time = s.split(":");
        String hh= time[0];
        String mm= time[1];
        String ss= (time[2]).substring(0,2);
        String m= (time[2]).substring(2,4);
        if(m.equals("PM")){
            hh= String.valueOf(Integer.parseInt(hh)+12);
            if(hh.equals("24"))hh="00";
        }else if(m.equals("AM")&& hh.equals("12")){
            hh ="00";
        }

        return hh+":"+mm+":"+ss;

    }

    public static void plusMinus(List<Integer> arr) {
        int positive=0;
        int negative=0;
        int zero=0;
        for(int num : arr){
            if (num>0){
                positive++;
            }else if(num<0){
                negative++;
            }else{
                zero++;
            }
        }

        System.out.println(arr.size()/positive);
        System.out.println(arr.size()/negative);
        System.out.println(arr.size()/zero);

    }

    public static int diagonalDifference(List<List<Integer>> arr) {
        int resultA=0;
        int resultB=0;
        int indexA=0;
        int indexB=(arr.get(0)).size()-1;
        for(int i=0;i<arr.size();i++ ){
            resultA+=(arr.get(i)).get(indexA);
            indexA++;
            resultB+=(arr.get(i)).get(indexB);
            indexB--;
        }
        return Math.abs(resultA - resultB);
    }


    public static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        int aScore=0;
        int bScore=0;
        for(int i=0;i<a.size();i++){
            if ((a.get(i) > b.get(i))) {
                aScore++;
            } else {
                bScore++;
            }
        }
        return Arrays.asList(aScore,bScore);
    }

    public static String compressedString(String message) {
        String result="";
        String last="";
        List<String> strArray = Arrays.asList(message.split(""));
        int aux=1;
        for(int i=0;i<strArray.size();i++ ){

            if(!(strArray.get(i).equals(last))){
                if(aux>1){
                    result+=+aux;
                    aux=1;
                }

                result+= strArray.get(i);
            }else {
                aux ++;
                if(i == strArray.size()-1)result+=aux;
            }
            last=strArray.get(i);

        }
        return result;
    }


}
